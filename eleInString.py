sentence = "Free Palestine"
search_char = input("Enter a char to search for: ")

indices = [i for i, char in enumerate(sentence) if char == search_char]

print(f"{search_char} found {len(indices)} time(s)")
for i, index in enumerate(indices):
    print(f"Occurrence {i+1} at index {index}")
