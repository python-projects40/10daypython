def is_palindrome(word_to_check) :
    n = len(word_to_check)

    # Check if length is even
    if n % 2 == 0:
        # Even length, split in half
        first_half = word_to_check[: n // 2]
        second_half = word_to_check[n // 2 :]
    else:
        # Odd length, split and exclude middle
        middle = n // 2
        first_half = word_to_check[:middle]
        second_half = word_to_check[middle + 1 :]

    # debugging statements
    second_half = second_half[::-1]
    print(f"First half: {first_half}")
    print(f"Second half: {second_half}")

    # Compare halves
    if first_half == second_half:
        return True
    else:
        return False

list_of_strings = [
    "tacocat" ,
    "jump",
    "testset",
    "William",
]
for word in list_of_strings:
    check = is_palindrome(word)
    if check:
        print("Yes")
    else: 
        print("No")
