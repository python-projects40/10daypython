# List of grocery items with dictionaries containing item_name, price, and quantity
grocery_list = [
    {"item_name": "Apples", "price": 1.5, "quantity": 10},
    {"item_name": "Donut", "price": 1.4, "quantity": 6},
    {"item_name": "Milk", "price": 5.0, "quantity": 1},
    {"item_name": "Spaghetti", "price": 2.5, "quantity": 2},
    {"item_name": "Eggs", "price": 4.0, "quantity": 1}
]
money_on_hand = 35.0  # Initial amount of money on hand
cart = []  # List to put your dictionaries (groceries) in 
#create a function to buy item
def buy_item(item):
    global money_on_hand
    global cart
    #check if you can 
    if money_on_hand >= item["price"]*item["quantity"]:
        cart.append(item)
        money_on_hand -= item["price"]*item["quantity"]
    else:
        affordable_qty = money_on_hand//item["price"]
        if affordable_qty > 0:
            item["quantity"] = affordable_qty
            cart.append(item)
            money_on_hand -= affordable_qty*item["price"]
#loop through items and call buy_item         
for item in grocery_list:
    buy_item(item)
#some print statements to give you a hand with the console
for item in cart:
    print(f" {item['quantity']} {item['item_name']}")
print(f"Money leftover: {money_on_hand}")