import random

# Step 1: Assign a variable to a random integer between 1 and 10
correct_number = random.randint(1, 10)

# Step 2: Ask the user to guess a number between 1 and 10 within 3 tries
tries = 3
for i in range(tries):
    user_guess = int(input("Guess a number between 1 and 10 in 3 tries: "))

    # Add hint for the first guess
    if i == 0 and user_guess > correct_number:
        print("Not bad for a first guess! Try something lower!⬇️")
    elif i == 0 and user_guess < correct_number:
        print("Not bad for a first guess, try something higher!⬆️")

    # Step 3: Cast the number the user gave you into an int
    user_guess = int(user_guess)

    # Step 4: If the user's number equals the correct number
    if user_guess == correct_number:
        # Step 5: Print an awesome message to the screen
        print("Congratulations!🥳 You guessed the number correctly.👏")
        break
    else:
        # Step 6: If not, print a message saying that the guess was incorrect

        # Step 7: If the guess is within 1, print a message they are getting HOT
        if abs(user_guess - correct_number) == 1:
            print("You are getting HOT! 🔥")
        # Step 8: If the number is within 2, print a message saying they are getting warmer
        elif abs(user_guess - correct_number) <= 2:
            print("You are getting warmer! ✨")
        # If the next guess is greater than 2, print a message they are getting colder
        else:
            print("You are getting colder! 🥶")

        # Make sure you count down the number of tries left
        print(f"You have {tries - i - 1} tries left.")

# If the user is unable to guess the correct number, print a message saying Sorry 😭 but you ran out of tries
if user_guess != correct_number:
    print(
        f"Sorry 😭 but you ran out of tries. The correct number was 👉 {correct_number} 👈")
