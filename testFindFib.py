import unittest
from findFib100 import i


class TestFib(unittest.TestCase):

    def test_fib_calculation(self):
        # Normal case
        self.assertEqual(i, 144)

        # Edge cases
        self.assertNotEqual(i, 0)
        self.assertNotEqual(i, 1)


if __name__ == '__main__':
    unittest.main()
