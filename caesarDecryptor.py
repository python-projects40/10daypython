
# Caesar Cipher Code Challenge
# I will give you a string, and you must print out the Caesar Cipher of that string. The string is defined below, but it is:
# "buqhdydw xem je setu yi byau q cqwys jhysa. edsu oek kdtuhijqdt, yj yi dej jxqj ycfhuiiylu, rkj kdjyb oek te, yj yi cqwys."
# Research Caesar Cipher if you need to! Here is a helpful image that won't tempt you to look at code:
# https://en.wikipedia.org/wiki/Caesar_cipher#/media/File:Caesar_cipher_left_shift_of_3.svg
#Don't cheat or give up early, and don't go too long and get frustrated or discouraged.

encrypted_message = "buqhdydw fojxed yi byau buqhdydw q cqwys jhysa. edsu oek adem xem yj mehai, yj iuuci iycfbu, rkj kdjyb oek te, yj'i cqwys!"
decrypted_alphabet = "abcdefghijklmnopqrstuvwxyz"
encrypted_alphabet1 = "klmnopqrstuvwxyzabcdefghij"
encrypted_alphabet2 = "qrstuvwxyzabcdefghijklmnop"
decrypted_message = ""
caesar_dict10 = {
    "a": "k",
    "b": "l",
    "c": "m",
    "d": "n",
    "e": "o",
    "f": "p",
    "g": "q",
    "h": "r",
    "i": "s",
    "j": "t",
    "k": "u",
    "l": "v",
    "m": "w",
    "n": "x",
    "o": "y",
    "p": "z",
    "q": "a",
    "r": "b",
    "s": "c",
    "t": "d",
    "u": "e",
    "v": "f",
    "w": "g",
    "x": "h",
    "y": "i",
    "z": "j"
}


def decrypt_function(encrypted_letter):
    #your function code here
    if encrypted_letter in caesar_dict10:
        return caesar_dict10[encrypted_letter]
    else:
        return encrypted_letter
    
#loop here
for char in encrypted_message:
    if char in caesar_dict10:
        decrypted_message += caesar_dict10[char]
    else:
        decrypted_message += char
print(decrypted_message)