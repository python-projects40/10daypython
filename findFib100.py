#import heartrate
#heartrate.trace(browser=True)
# Usually we access a list element with list_name[i]
# You can access any element in a list anytime no matter what i is
# For instance list_name[i+1] or list_name[i*3]...

list_of_fib = [0, 1]

# i should be the sequence number you are calculating
i = 0
def append_and_print_fib(i):
    # code here
    i = list_of_fib[-2] + list_of_fib[-1]
    list_of_fib.append(i)


# loop here
# call the function and send the sequence number to calculate
while True:
    append_and_print_fib(i)
    i +=1 
    # Exit condition
    if i == 100:
        print(list_of_fib[i])
        break
